(function($){
 
    $.fn.extend({ 
         
        //pass the options variable to the function
        fmnavslider: function(options) {
 
 
            //Set the default values, use comma to separate the settings, example:
            var defaults = {
				marker : '.pointer',
				active : '.active'
            };
                 
            var options =  $.extend(defaults, options);
 
            return this.each(function() {
                var o = options;
				var root = $(this);
                var active = $(this).find(o.active + ' a');
				var a = $(this).find(o.active + ' a');
				var p = a.position();
				var w = a.width();
				var mx = p.left;
				var x = mx+(w/2);
				var s = 300; // speed
				
				slidemarker(x);
				
				
				$(root).find('a').hover(function(){

					// get the position and width of the active element
					var a = $(this);
					var p = a.position();
					var w = a.width();
					var mx = p.left;
					var x = mx+(w/2);
					
					// position the marker
					slidemarker(x);

				});
				
				
				$(root).find('a').mouseout(function(){
					// when we mouseout return the marker to the active element
					var a = $(root).find('.active a');
					var p = a.position();
					var w = a.width();
					var mx = p.left;
					var x = mx+(w/2);

					// position the marker
					slidemarker(x);
				});
				
				
				function slidemarker(x){
					// position the marker
					var m = $(root).find(o.marker);
					var mw = m.width();
					m.animate({
						'left':x+'px'
					},s).clearQueue();
				}
				
            });

				

        }
    });
     
})(jQuery);