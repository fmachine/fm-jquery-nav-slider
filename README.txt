A simple jQuery plugin to create a simple navigation slider.
The slider will track your mouse as you hover over navigation elements and "stick" on the selected element.

Demo: http://www.fantasticmachine.co.uk/demos/navslider/